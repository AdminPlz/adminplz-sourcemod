/*
AdminPLZ!

by Jouva Moufette <jouva@moufette.com>
*/

#pragma semicolon 1
#pragma newdecls required
#include <sourcemod>
#include <ripext>

public Plugin myinfo =
{
        name = "AdminPLZ!",
        author = "Jouva Moufette <jouva@moufette.com>",
        description = "Summons server admins via push notifications",
        version = "1.0",
        url = "http://adminplz.com/"
}

char g_api_id[17];
char g_api_key[129];

HTTPClient httpClient;

public void OnPluginStart()
{
    LoadTranslations("adminplz.phrases");
    LoadConfig();
    RegConsoleCmd("sm_adminplz", Command_AdminPlz);
    RegAdminCmd("sm_adminplz_reload", Command_AdminPlzReload, ADMFLAG_RCON);
}

public Action Command_AdminPlz(int client, int args)
{
    JSONObject requestBody = new JSONObject();
    requestBody.SetString("auth_id", g_api_id);
    requestBody.SetString("auth_key", g_api_key);
    
    httpClient = new HTTPClient("https://adminplz.com/");
    httpClient.Post("api/admins/alert", requestBody, OnAdminRequested);
    
    PrintToChat(client, "[AdminPLZ!] %T", "Requesting admin", client);
    
    return Plugin_Handled;
}

public void OnAdminRequested(HTTPResponse response, any value)
{
    if (response.Status != HTTPStatus_NoContent) {
        PrintToServer("Failed to request admin: Did not get expected response");
        delete httpClient;
        return;
    }
}

public Action Command_AdminPlzReload(int client, int args)
{
    LoadConfig();
    PrintToChat(client, "[AdminPLZ!] %T", "Config reloaded", client);
    return Plugin_Handled;
}

bool LoadConfig()
{
    char strConfigPath[256];

    BuildPath(Path_SM, strConfigPath, sizeof(strConfigPath), "configs/adminplz.cfg");
    KeyValues kv = new KeyValues("AdminPlzConfig");
    kv.ImportFromFile(strConfigPath);

    if (!kv.JumpToKey("api")) {
       delete kv;
       return false;
    }
    kv.GetString("id", g_api_id, sizeof(g_api_id));
    kv.GetString("key", g_api_key, sizeof(g_api_key));
    delete kv;

    return true;
}
